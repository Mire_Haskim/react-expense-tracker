import React, {useState} from "react";
import ExpenseRenderer from "./Components/Expenses/ExpenseItemRenderer";
import NewExpense from "./Components/NewExpenses/NewExpense";
function App() {

  const [expenses, setexpenses] = useState([
    {
      id:"e1",
      title:"Toilet paper",
      amount:55.00,
      date:new Date(2021,5,1),
    },
    {
      id:"e2",
      title:"Movie Ticket",
      amount:67.65,
      date:new Date(2021,4,18),
    },
    {
      id:"e3",
      title:"Grocery",
      amount:378.98,
      date:new Date(2021,0,24),
    },
    {
      id:"e4",
      title:"Graphics Card",
      amount:699.99,
      date:new Date(2020,1,24),
    },
  ]);

  const addExpenseHandler = (expense) => {
    setexpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });

  };



  return (
    <div className="App">
      <NewExpense onAddExpense = {addExpenseHandler}/>
      <ExpenseRenderer expenses = {expenses} />
    </div>
  );
}

export default App;


