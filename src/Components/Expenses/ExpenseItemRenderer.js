import React, {useState}from 'react';
import ExpenseItem from './ExpenseItem';
import './ExpenseRenderer.css';
import ExpensesFilter from './ExpensesFilter';
import ExpensesChart from './ExpensesChart';

export default function ExpenseRenderer(props){

  const [selectedYear, setSelectedYear] = useState('2020');

  const filterChangeHandler = (selectedYear) => {
      setSelectedYear(selectedYear);
  };

  const filteredExpenses = props.expenses.filter((expense) => {
    return expense.date.getFullYear().toString() === selectedYear;
  } );

    return (
    <div className="expenses">
      <ExpensesFilter defaultYear = {selectedYear} filterChangeHandler = {filterChangeHandler}/>
      <ExpensesChart expense = {filteredExpenses}/>
      {filteredExpenses.map((expense) => (<ExpenseItem key = {expense.id} date ={expense.date} type={expense.title} price={expense.amount}/>))}
    </div>
    );
}